#
# Copyright (C) 2025 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

PRODUCT_MAKEFILES := \
    $(LOCAL_DIR)/lineage_rhodei.mk

COMMON_LUNCH_CHOICES := \
    lineage_rhodei-user \
    lineage_rhodei-userdebug \
    lineage_rhodei-eng
