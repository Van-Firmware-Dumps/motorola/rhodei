#
# Copyright (C) 2025 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

# Inherit from those products. Most specific first.
$(call inherit-product, $(SRC_TARGET_DIR)/product/core_64_bit.mk)
$(call inherit-product, $(SRC_TARGET_DIR)/product/full_base_telephony.mk)

# Inherit some common Lineage stuff.
$(call inherit-product, vendor/lineage/config/common_full_phone.mk)

# Inherit from rhodei device
$(call inherit-product, device/motorola/rhodei/device.mk)

PRODUCT_DEVICE := rhodei
PRODUCT_NAME := lineage_rhodei
PRODUCT_BRAND := motorola
PRODUCT_MODEL := moto g62 5G
PRODUCT_MANUFACTURER := motorola

PRODUCT_GMS_CLIENTID_BASE := android-motorola

PRODUCT_BUILD_PROP_OVERRIDES += \
    PRIVATE_BUILD_DESC="rhodei_g-user 13 T1SSIS33M.1-75-7-12 559ad release-keys"

BUILD_FINGERPRINT := motorola/rhodei_g/rhodei:13/T1SSIS33M.1-75-7-12/559ad:user/release-keys
