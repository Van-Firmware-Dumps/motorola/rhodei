#
# Copyright (C) 2025 The Android Open Source Project
# Copyright (C) 2025 SebaUbuntu's TWRP device tree generator
#
# SPDX-License-Identifier: Apache-2.0
#

PRODUCT_MAKEFILES := \
    $(LOCAL_DIR)/omni_rhodei.mk

COMMON_LUNCH_CHOICES := \
    omni_rhodei-user \
    omni_rhodei-userdebug \
    omni_rhodei-eng
